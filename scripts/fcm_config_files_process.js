#!/usr/bin/env node
'use strict';

var fs = require('fs');

if (fs.existsSync('platforms/android/')) {

    var googleServicesJson = "google-services.json";

    if (fs.existsSync(googleServicesJson)) {
        var contents = fs.readFileSync(googleServicesJson).toString();
        fs.writeFileSync("platforms/android/app/google-services.json", contents);
    }

    //
    // Make the main Activity extend from the FCM CordovaActivity
    //
    // First, we need the app package
    var androidManifestPath = 'platforms/android/app/src/main/AndroidManifest.xml';
    var androidManifestContent = fs.readFileSync(androidManifestPath).toString();
    var re = /package=\s*"([^"]*)"/gi;

    var m = re.exec(androidManifestContent);
    if (m !== null) {
        var packageName = m[1];
    }

    var mainActivityPath = 'platforms/android/app/src/main/java/' + packageName.replace(/\./g, '/') + '/MainActivity.java';
    process.stdout.write('Making main activity ' + mainActivityPath + ' extend from the FCM CordovaActivity\n');
    if (fs.existsSync(mainActivityPath)) {
        var mainActivityJava = fs.readFileSync(mainActivityPath).toString();
        mainActivityJava = mainActivityJava.replace('import org.apache.cordova.*;',
            'import com.quobis.fcm.plugin.CordovaActivity;');
        fs.writeFileSync(mainActivityPath, mainActivityJava);
    } else {
        process.stdout.write('Main class file not found :(\n');
    }
}

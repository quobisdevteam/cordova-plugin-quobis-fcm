This is a modification of https://github.com/fechanique/cordova-plugin-fcm to achieve the required
behaviour of our mobile webphone:
  
* Do nothing if the app is running in foreground
* Bring the app to front if the app is not running in foreground
* If the mobile phone's screen is off, turn on the screen and launch the app in foreground
* If the mobile has a lock screen, put the main app over the lock screen

The Cordova main activity must extend from CordovaWakeUpActivity.

It is only available for android.

Permissions
===========

This plugin adds 2 permissions to the application

* android.permission.WAKE_LOCK
* android.permission.DISABLE_KEYGUARD


Registering a topic
===================
The app must register a topic calling the subscribeToTopic() method of the plugin. 

The initial version of the plugin registers the app to the "android" topic.
  
Sending Messages to the app
===========================
The messages must be sent with a POST to the endpoint: https://fcm.googleapis.com/fcm/send
  
Messages must be of the "data" type, with a "title" and "message" fields like this
  
``` 
   {
     "to" : "/topics/android",
      "data" : {
        "title" : "Incoming call",
        "message" : "Alice is calling!",
      }
  }
```
The POST messages must contain the headers:

* Authorization: key=xxxxxxxx
* Content-Type: application/json
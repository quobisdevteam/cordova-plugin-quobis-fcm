package com.quobis.fcm.plugin;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class QuobisFirebaseInstanceIdService extends FirebaseInstanceIdService {
	private static final String TAG = QuobisFirebaseInstanceIdService.class.getSimpleName();

	@Override
	public void onTokenRefresh(){
		String refreshedToken = FirebaseInstanceId.getInstance().getToken();
		Log.d(TAG, "Refreshed token: " + refreshedToken);
		FCMPlugin.sendTokenRefresh( refreshedToken );
	}
}

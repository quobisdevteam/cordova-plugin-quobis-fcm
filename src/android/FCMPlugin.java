package com.quobis.fcm.plugin;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Map;

public class FCMPlugin extends CordovaPlugin {
	private static final String TAG = "QuobisFCMPlugin";

	public static CordovaWebView gWebView;
	public static String notificationCallBack = "FCMPlugin.onNotificationReceived";
	public static String tokenRefreshCallBack = "FCMPlugin.onTokenRefreshReceived";
	public static Boolean notificationCallBackReady = false;
	public static Map<String, Object> lastPush = null;
	public static NotificationManager notificationManager;
	public static Context context;
	public static String NOTIFICATION_CHANNEL_ID = QuobisFirebaseMessagingService.class.getPackage().getName();
	public static String CALLS_CHANNEL_ID = "Calls channel";


	private static boolean notDisturbCalls = false;
	private static boolean notDisturbChat = false;


	public FCMPlugin() {
	}

	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		gWebView = webView;
		Log.w(TAG, "==> FCMPlugin initialize");
		//FirebaseMessaging.getInstance().subscribeToTopic("android");
		context = this.cordova.getActivity().getApplicationContext();
		notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			createCallNotificationChannel(CALLS_CHANNEL_ID);
			createNotificationChannel();
		}
	}

	/**
	 * Execute one supported action on FCMPlugin
	 * @param action          The action to execute.
	 * @param args            The exec() arguments.
	 * @param callbackContext The callback context used when calling back into JavaScript.
	 * @return If one action was executed
	 * @throws JSONException
	 */
	public boolean execute(final String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
		Log.d(TAG, "==> FCMPlugin execute: " + action);

		try {
			switch (action) {
				case "ready":
					callbackContext.success();
					break;
				case "doNotDisturb":
					notDisturbCalls = args.getJSONObject(0).getBoolean("callPush");
					notDisturbChat = args.getJSONObject(0).getBoolean("chatPush");
					// Allow to save and retrieve data in the form of key,value pair.
					// Used to restore the do not disturb value when app is killed.
					SharedPreferences pushStatus = PreferenceManager.getDefaultSharedPreferences(context);
					SharedPreferences.Editor editor = pushStatus.edit();
					editor.putBoolean("call", notDisturbCalls);
					editor.putBoolean("chat", notDisturbChat);
					editor.commit();
					QuobisFirebaseMessagingService.updateDndValues(notDisturbCalls, notDisturbChat);
					break;
				case "getToken":
					cordova.getActivity().runOnUiThread(new Runnable() {
						public void run() {
							try {
								String token = FirebaseInstanceId.getInstance().getToken();
								callbackContext.success(FirebaseInstanceId.getInstance().getToken());
								Log.d(TAG, "\tToken: " + token);
							} catch (Exception e) {
								Log.d(TAG, "\tError retrieving token");
							}
						}
					});
				case "registerNotification":
					notificationCallBackReady = true;
					cordova.getActivity().runOnUiThread(new Runnable() {
						public void run() {
							if (lastPush != null) {
								FCMPlugin.sendPushPayload(lastPush);
							}
							lastPush = null;
							callbackContext.success();
						}
					});
					break;
				case "subscribeToTopic":
					cordova.getThreadPool().execute(new Runnable() {
						public void run() {
							try {
								FirebaseMessaging.getInstance().subscribeToTopic(args.getString(0));
								callbackContext.success();
							} catch (Exception e) {
								callbackContext.error(e.getMessage());
							}
						}
					});
					break;
				case "unsubscribeFromTopic":
					cordova.getThreadPool().execute(new Runnable() {
						public void run() {
							try {
								FirebaseMessaging.getInstance().unsubscribeFromTopic(args.getString(0));
								callbackContext.success();
							} catch (Exception e) {
								callbackContext.error(e.getMessage());
							}
						}
					});
					break;
				case "destroyNotification":
					QuobisFirebaseMessagingService.removeCallNotification();
					break;
				case "closeCallNotification":
					QuobisFirebaseMessagingService.closeCallLockedScreen(context);
				default:
					callbackContext.error("Method not found");
					return false;
			}
		} catch (Exception e) {
			Log.d(TAG, "ERROR: onPluginAction: " + e.getMessage());
			callbackContext.error(e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Send push Payload to WebView
	 * @param payload payload
	 */
	public static void sendPushPayload(Map<String, Object> payload) {
		Log.d(TAG, "==> FCMPlugin sendPushPayload");
		Log.d(TAG, "\tnotificationCallBackReady: " + notificationCallBackReady);
		Log.d(TAG, "\tgWebView: " + gWebView);
		try {
			JSONObject jo = new JSONObject();
			for (String key : payload.keySet()) {
				jo.put(key, payload.get(key));
				Log.d(TAG, "\tpayload: " + key + " => " + payload.get(key));
			}
			String callBack = "javascript:" + notificationCallBack + "(" + jo.toString() + ")";
			if (notificationCallBackReady && gWebView != null) {
				Log.d(TAG, "\tSent PUSH to view: " + callBack);
				gWebView.sendJavascript(callBack);
			} else {
				Log.d(TAG, "\tView not ready. SAVED NOTIFICATION: " + callBack);
				lastPush = payload;
			}
		} catch (Exception e) {
			Log.d(TAG, "\tERROR sendPushToView. SAVED NOTIFICATION: " + e.getMessage());
			lastPush = payload;
		}
	}

	public static void sendTokenRefresh(String token) {
		Log.d(TAG, "==> FCMPlugin sendRefreshToken");
		try {
			String callBack = "javascript:" + tokenRefreshCallBack + "('" + token + "')";
			gWebView.sendJavascript(callBack);
		} catch (Exception e) {
			Log.d(TAG, "\tERROR sendRefreshToken: " + e.getMessage());
		}
	}

	@Override
	public void onDestroy() {
		gWebView = null;
		notificationCallBackReady = false;
	}

	/**
	 * Create the notification channel of Call notifications for smart phones with api 26 or higher
	 * @return Channel ID
	 */
	@RequiresApi(Build.VERSION_CODES.O)
	private static String createCallNotificationChannel(String channelId) {
		NotificationChannel notificationChannel = new NotificationChannel(
				channelId,
				channelId,
				NotificationManager.IMPORTANCE_HIGH
		);
		notificationChannel.setDescription(channelId);
		notificationChannel.enableVibration(true);
		notificationManager.createNotificationChannel(notificationChannel);
		return channelId;
	}

	/**
	 * Create the notification channel for smart phones with api 26 or higher
	 * @return Channel ID
	 */
	@RequiresApi(Build.VERSION_CODES.O)
	private String createNotificationChannel() {
		String channelId = NOTIFICATION_CHANNEL_ID;
		NotificationChannel channel = new NotificationChannel(
				NOTIFICATION_CHANNEL_ID,
				NOTIFICATION_CHANNEL_ID,
				NotificationManager.IMPORTANCE_HIGH);
		channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
		channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
		notificationManager.createNotificationChannel(channel);
		return channelId;
	}
}

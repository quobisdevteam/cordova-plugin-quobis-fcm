package com.quobis.fcm.plugin;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.cordova.CordovaWebView;

public class CordovaActivity extends org.apache.cordova.CordovaActivity {
	public static final String TAG = "QuobisFCMActivity";

	// Put to true before launching the intent
	public static boolean setWindowFlags = true;
	// When the activity is started with the screen off, it is going to be paused and resumed again
	// Do not stop the webview in these cases
	public static boolean keepWebViewRunning = false;
	public static boolean moveTaskToBack = false;

	public static boolean initialized = false;
	// Used from the service to avoid relaunching the activity if already in foreground
	public static boolean foreground = false;
	public static boolean webViewRunning = false;

	// When we must shown an intermediate screen with the incoming call
	public static String showIncomingCall = null;
	public static boolean showingIncomingCall = false;

	public static CordovaActivity instance = null;

	boolean hasWindowFlags = false;

	public Handler hideIncomingCallHandler = new Handler(Looper.getMainLooper()) {
		@Override
		public void handleMessage(Message inputMessage) {
			hideIncomingCall();
		}
	};

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		initialized = true;
		instance = this;
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		Log.d(TAG, "onNewIntent");
		super.onNewIntent(intent);

		if (moveTaskToBack) {
			moveTaskToBack = false;
			moveTaskToBack(true);
		}
	}

	protected void onResume() {
		Log.d(TAG, "onResume");
		foreground = true;

		if (setWindowFlags) {
			setWindowFlags = false;
			setWindowFlags();
		}

		// Avoid calling cordova's onResume if pausing automatically
		CordovaWebView appViewBackup = null;
		if (keepWebViewRunning && webViewRunning) {
			Log.d(TAG, "onResume: webView ignoring resume");
			appViewBackup = appView;
			appView = null;
		}
		super.onResume();
		if (keepWebViewRunning && webViewRunning) {
			appView = appViewBackup;
		}

		webViewRunning = true;

		if (moveTaskToBack) {
			moveTaskToBack = false;
			moveTaskToBack(true);
		}

		if (showIncomingCall != null) {
			showIncomingCall(showIncomingCall);
		} else {
			hideIncomingCall();
		}
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause: keepWebViewRunning=" + keepWebViewRunning);

		// Avoid calling cordova's onPause if pausing automatically
		CordovaWebView appViewBackup = null;
		if (keepWebViewRunning) {
			appViewBackup = appView;
			appView = null;
		}
		super.onPause();
		if (keepWebViewRunning) {
			appView = appViewBackup;
		} else {
			removeWindowFlags();
			webViewRunning = false;
		}
		foreground = false;
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		initialized = false;
		instance = null;
		super.onDestroy();
	}

	/**
	 * Brings the app to front, if the screen is locked, it sets window flags to put the app OVER the lock screen
	 */
	public void setWindowFlags() {
		if (!hasWindowFlags) {
			Log.d(TAG, "setWindowFlags");
			hasWindowFlags = true;
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
					WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
					WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
					WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	public void removeWindowFlags() {
		if (hasWindowFlags) {
			Log.d(TAG, "removeWindowFlags");
			hasWindowFlags = false;
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
					WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
					WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
					WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	public void showIncomingCall(String caller) {
		Log.d(TAG, "showIncomingCall");
		if (!showingIncomingCall) {
			String contactName = getContactName(caller);
			if (contactName == null) {
				contactName = caller;
			}

			int imageResourceId = getResources().getIdentifier("incoming_call", "drawable", getPackageName());
			if (imageResourceId == 0) {
				// We only show the incoming call if the cordova project has an incoming_call drawable resource
				return;
			}

			showingIncomingCall = true;

			LinearLayout linearLayout = new LinearLayout(this);
			linearLayout.setOrientation(LinearLayout.VERTICAL);
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					WindowManager.LayoutParams.MATCH_PARENT,
					WindowManager.LayoutParams.MATCH_PARENT);
			linearLayout.setLayoutParams(layoutParams);
			linearLayout.setGravity(Gravity.CENTER);
			linearLayout.setBackgroundColor(Color.parseColor("#000000"));

			ImageView imageView = new ImageView(this);
			imageView.setImageResource(imageResourceId);
			LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams(300, 300);
			imageView.setLayoutParams(imageLayoutParams);
			linearLayout.addView(imageView);

			TextView textView = new TextView(this);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
			textView.setGravity(Gravity.CENTER);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			textView.setLayoutParams(params);
			textView.setText("שיחה נכנסת");
			textView.setTextColor(Color.parseColor("#aaaaaa"));
			linearLayout.addView(textView);

			TextView callerTextView = new TextView(this);
			callerTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
			callerTextView.setGravity(Gravity.CENTER);
			callerTextView.setLayoutParams(params);
			callerTextView.setText(contactName);
			callerTextView.setTextColor(Color.parseColor("#aaaaaa"));
			linearLayout.addView(callerTextView);

			setContentView(linearLayout);

			hideIncomingCallHandler.sendMessageAtTime(hideIncomingCallHandler.obtainMessage(0),
					SystemClock.uptimeMillis() + 20000);
		}
	}

	public void hideIncomingCall() {
		Log.d(TAG, "hideIncomingCall");
		if (showingIncomingCall) {
			showingIncomingCall = false;
			showIncomingCall = null;
			setContentView(appView.getView());
			hideIncomingCallHandler.removeMessages(0);
		}
	}

	public String getContactName(final String phoneNumber) {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
			return null;
		}
		Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
		String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
		String contactName = null;
		Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				contactName = cursor.getString(0);
			}
			cursor.close();
		}
		return contactName;
	}
}

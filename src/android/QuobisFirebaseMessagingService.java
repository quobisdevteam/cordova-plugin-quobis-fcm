package com.quobis.fcm.plugin;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class QuobisFirebaseMessagingService extends FirebaseMessagingService {
	public static boolean callPushStatus;
	public static boolean chatPushStatus;
	public static Timer removeKeepWebViewRunningTimer = new Timer();

	private static final String TAG = QuobisFirebaseMessagingService.class.getSimpleName();
	private static final int NOTIFICATION_ID = 0xefe0;
	private static final int CALL_NOTIFICATION_ID = 1;
	private static String lastMessageData = null;
	private static boolean dndCalls;
	private static boolean dndChat;
	private static NotificationManager notificationManager;
	private static boolean openedCallNotification = false;
	private Context context;

	// It's necessary the app context to retrieve the values of the do not disturb.
	public Context classContext(Context context){
		return this.context = context;
	}

	/**
	 * Called when message is received.
	 *
	 * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
	 */
	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		String messageData = remoteMessage.getData().get("data");
		Log.d(TAG, "Push received, data: " + remoteMessage.getData());
		SharedPreferences pushStatus = PreferenceManager.getDefaultSharedPreferences(classContext(this));
		callPushStatus = pushStatus.getBoolean("call", false);
		chatPushStatus = pushStatus.getBoolean("chat", false);

		if (QuobisFirebaseMessagingService.lastMessageData != null &&
				QuobisFirebaseMessagingService.lastMessageData.equals(messageData)) {
			Log.d(TAG, "Duplicated message payload, ignoring push request");
		} else {
			boolean isPing = false;
			boolean isHangup = false;
			boolean isNotification = false;
			String showIncomingCall = null;
			try {
				if (remoteMessage.getData().containsKey("payload") && (!dndCalls || !callPushStatus)) {
					JSONObject payload = new JSONObject(remoteMessage.getData().get("payload"));
					if (payload.has("action")) {
						String action = payload.getString("action");
						isPing = "ping".equals(action);
						isHangup = "hangup".equals(action);
					}
					if (payload.has("phone")) {
						showIncomingCall = payload.getString("phone");
					}
					if (payload.has("type") && payload.getString("type").equals("meeting-reminder")) {
						isNotification = true;
						JSONObject info = payload.getJSONObject("info");
						NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
						notificationManager.notify(NOTIFICATION_ID, createNotification(info.getString("name"), null));
					}
				} else if (remoteMessage.getData().containsKey("type") && remoteMessage.getData().get("type").equals("xmpp")) {
					Log.d(TAG, "XMPP push received");
					if (dndChat || chatPushStatus) {
						isNotification = true;
						return;
					}
					// Do not show XMPP pushes when the app is in foreground
					if (CordovaActivity.foreground) {
						Log.d(TAG, "Ignoring XMPP push, the app is in foreground");
						return;
					}
					String sender = prepareXmppSender(remoteMessage.getData().get("sender"));
					String body = remoteMessage.getData().get("body");
					isNotification = true;
					notify(NOTIFICATION_ID, createNotification(sender, body));
				}
			} catch (Exception e) {
			}

			if (!isPing && !isHangup && !isNotification && (!dndCalls || !callPushStatus) && !CordovaActivity.foreground) {
				Map<String, Object> data = new HashMap<String, Object>();
				for (String key : remoteMessage.getData().keySet()) {
					Object value = remoteMessage.getData().get(key);
					Log.d(TAG, "\tKey: " + key + " Value: " + value);
					data.put(key, value);
				}
				Log.d(TAG, "\tNotification Data: " + data.toString());
				FCMPlugin.sendPushPayload(data);
				removeCallNotification();
				launchCall(showIncomingCall,FCMPlugin.CALLS_CHANNEL_ID, false, 30000);
			}
		}
		QuobisFirebaseMessagingService.lastMessageData = messageData;
	}

	/**
	 * Change value of calls or chat DnD
	 * @param calls
	 * @param push
	 */
	public static void updateDndValues(boolean calls, boolean push) {
		dndCalls = calls;
		dndChat = push;
	}

	/**
	 * Removes the call notification
	 */
	public static void removeCallNotification() {
		if (notificationManager != null) {
			notificationManager.cancel(CALL_NOTIFICATION_ID);
		}
	}

	/**
	 * Close the Application if was opened for a call and the screen is locked
	 * @param ctx Context
	 */
	public static void closeCallLockedScreen(Context ctx) {
		if (openedCallNotification) {
			CordovaActivity.showIncomingCall = null;
			CordovaActivity.keepWebViewRunning = false;
			if (CordovaActivity.foreground) {
				CordovaActivity.moveTaskToBack = true;
				PackageManager packageManager = ctx.getPackageManager();
				Intent intent = packageManager.getLaunchIntentForPackage(ctx.getPackageName());
				ctx.startActivity(intent);
			}
			openedCallNotification = false;
		}
	}

	private static void wakeLock(Context ctx, String caller, boolean background, int timeout) {
		PowerManager powerManager = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
		CordovaActivity.setWindowFlags = true;
		CordovaActivity.keepWebViewRunning = true;
		CordovaActivity.moveTaskToBack = background;
		CordovaActivity.showIncomingCall = caller;
		PowerManager.WakeLock wakeLock = powerManager.newWakeLock(
				PowerManager.FULL_WAKE_LOCK |
						PowerManager.ACQUIRE_CAUSES_WAKEUP |
						PowerManager.ON_AFTER_RELEASE,
				"collaborator:lockTag.");
		wakeLock.acquire(timeout);
	}

	private static void removeKeepWebViewRunning(Context ctx) {
		if (CordovaActivity.keepWebViewRunning) {
			Log.d(TAG, "Removing the keepWebViewRunning flag");
			CordovaActivity.keepWebViewRunning = false;
			// If the app is not in foreground, we need to relaunch it to stop the webview
			if (!CordovaActivity.foreground) {
				CordovaActivity.moveTaskToBack = true;
				PackageManager packageManager = ctx.getPackageManager();
				Intent intent = packageManager.getLaunchIntentForPackage(ctx.getPackageName());
				ctx.startActivity(intent);
			}
		}
	}

	private String prepareXmppSender(String sender) {
		if (sender.indexOf('@') >= 0) {
			sender = sender.substring(0, sender.lastIndexOf('@'));
		}
		if (sender.indexOf('/') >= 0) {
			sender = sender.substring(sender.lastIndexOf('/') + 1);
		}
		return sender;
	}

	/**
	 * Create a simple notification with title and content text
	 * @param title Title of the notification
	 * @param text Text of the notification
	 * @return Builder notification
	 */
	private Notification createNotification(String title, String text) {
		Log.d(TAG, "Create notification " + title + " " + text);
		PackageManager packageManager = getPackageManager();
		Intent intent = packageManager.getLaunchIntentForPackage(getPackageName());
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this, FCMPlugin.NOTIFICATION_CHANNEL_ID)
				.setDefaults(Notification.DEFAULT_ALL)
				.setAutoCancel(true)
				.setPriority(NotificationCompat.PRIORITY_HIGH)
				.setContentIntent(pendingIntent); // Required on Gingerbread and below

		if (title != null) {
			builder.setContentTitle(title);
		}
		if (text != null) {
			builder.setContentText(text);
		}

		int imageResourceId = getResources().getIdentifier("notification_icon", "drawable", getPackageName());
		if (imageResourceId != 0) {
			Log.d(TAG, "Setting notification icon " + imageResourceId);
			builder.setBadgeIconType(Notification.BADGE_ICON_SMALL);
			builder.setSmallIcon(imageResourceId);
		}
		Log.d(TAG, "created notification " + builder);
		return builder.build();
	}

	/**
	 * Launch a notification with Full Screen Intent to show the App Activity when is received a call push notification
	 * @param caller Name of the caller to show in the notification
	 * @param channelId Id of the channel in which we want to show the notification
	 * @param background wake lock background
	 * @param timeout wake lock timeout
	 */
	private void launchCall(String caller, String channelId, boolean background, int timeout) {
		if (callPushStatus) {
			return;
		}
		Log.d(TAG, "launchCall: " + background + ", " + timeout);
		removeKeepWebViewRunningTimer.purge();
		removeKeepWebViewRunningTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				removeKeepWebViewRunning(context);
			}
		}, timeout);

		if (!CordovaActivity.foreground) {
			wakeLock(context, caller, background, timeout);
			int incomingCallNotification = getResources().getIdentifier("incoming_call", "string", getPackageName());
			int notificationIcon = getResources().getIdentifier("notification_icon", "drawable", getPackageName());
			try {
				Intent fullScreenIntent = new Intent(context, Class.forName(getPackageName() + ".MainActivity"));
				PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				Notification notification = new NotificationCompat.Builder(context, channelId)
						.setAutoCancel(true)
						.setCategory(Notification.CATEGORY_CALL)
						.setVisibility(Notification.VISIBILITY_PUBLIC)
						.setSmallIcon(notificationIcon)
						.setPriority(NotificationCompat.PRIORITY_HIGH)
						.setContentTitle(context.getResources().getString(incomingCallNotification))
						.setContentText(caller)
						.setFullScreenIntent(pendingIntent, true)
						.build();
				notify(CALL_NOTIFICATION_ID, notification);
				openedCallNotification = true;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Show a lost call notification
	 * @param caller display name of the caller
	 */
	private void showLostCall(String caller) {
		int lostCallNotification = getResources().getIdentifier("lost_call", "string", getPackageName());
		Notification lostCall = createNotification(context.getResources().getString(lostCallNotification), caller);
		notify(NOTIFICATION_ID, lostCall);
	}

	/**
	 * Launch a notification
	 * @param id notification id
	 * @param notification notification to launch
	 */
	private void notify(int id, Notification notification) {
		notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(id, notification);
	}
}
